using System;
using UnityEngine;

namespace Asteroids
{
    public sealed class EnemyShip : Enemy

    {
        [SerializeField] private float damage = 20;
        [SerializeField] Player _player;

        [SerializeField] private float _speed = 1f;
        [SerializeField] private float _acceleration;
        
        [SerializeField] private Rigidbody2D _bullet;
        [SerializeField] private Transform _barrel;
        [SerializeField] private float _force = 10f;
        private Camera _camera;
        private Ship _ship;






        private  BulletView _bulletView;




        private void Start()
        {
          _player = GameObject.FindObjectOfType<Player>();
            var moveTransform = new AccelerationMove(transform, _speed,
            _acceleration);
            var rotation = new RotationShip(transform);
            _ship = new Ship(moveTransform, rotation);
        }
        private void Update()
        {
            if ((transform.position - _player.transform.position).magnitude < 5f)
            {
                var temAmmunition = Instantiate(_bullet, _barrel.position, _barrel.rotation);
                temAmmunition.AddForce(_barrel.up * _force);
            }



            var direction = transform.position - _player.transform.position;
            _ship.Rotation(direction);
            _ship.Move(_player.transform.position.x, _player.transform.position.y,  Time.deltaTime);


           
            
        }


       

        public void OnCollisionEnter2D(Collision2D other)
        {
            var _obj = other.transform.GetComponentInParent<BulletView>();
            if (_obj != null)
            {
                Destroy(gameObject);

            }
        }

        

       




        public float TakeDamage ()
        {
            return damage;
        }

     
    }
}
