using System;
using UnityEngine;

namespace Asteroids
{
    public abstract class Enemy : MonoBehaviour
    {

        public static IEnemyFactory Factory;
        public Health Health { get; protected set; }






        public static Asteroid CreateAsteroidEnemy (Health hp)
        {
            var enemy = Instantiate(Resources.Load<Asteroid>("Enemy/Asteroid"));
            enemy.Health = hp;
            return enemy;
        }



        public static Asteroid CreateAsteroidEnemy2Type(Health hp)
        {
            var enemy = Instantiate(Resources.Load<Asteroid>("Enemy/Asteroid2"));
            enemy.Health = hp;
            return enemy;
        }

        public static EnemyShip CreateEnemyShip(Health hp)
        {
            var enemy = Instantiate(Resources.Load<EnemyShip>("Enemy/EnemyShip"));
            enemy.Health = hp;
            // ����� � 0 ����
            enemy.transform.position = Vector3.zero;
            return enemy;
        }

        public void DependencyInjectHealth(Health hp)
        {
            Health = hp;
        }

       
        public void Move(float force, Vector2 direction)
        {
            var _enemyrb = GetComponentInParent<Rigidbody2D>();
            _enemyrb.AddForce(direction * force);
        }
    }
}