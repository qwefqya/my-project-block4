using System;
using System.Collections.Generic;
using UnityEngine;
namespace Asteroids
{
    internal sealed class Example : MonoBehaviour
    {
        [SerializeField] private Sprite _sprite;
        [SerializeField] private Rigidbody2D _rb;

      


        private void Start()
        {
            

            var GO = new
            GameObject().SetName("Bullet").AddBoxCollider2D().AddBoxCollider2D().AddRigidbody2D
            (5.0f).AddSprite(_sprite);
            _rb = GO.GetComponent<Rigidbody2D>();
            _rb.AddForce(Vector2.one);
        }
    }
}