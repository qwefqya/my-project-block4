using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asteroids
{
    internal sealed class AddHPModifier : PlayerModifier
    {
        private readonly float _maxHP;
        public AddHPModifier(Player player, float maxHP)
        : base(player)
        {
            _maxHP = maxHP;
        }
        public override void Handle()
        {
            if (_player._hp <= _maxHP)
            {
                _player._hp = _maxHP;
            }
            base.Handle();
        }
    }
}