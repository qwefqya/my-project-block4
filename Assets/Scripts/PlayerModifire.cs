using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asteroids
{
    internal class PlayerModifier
    {
        protected Player _player;
        protected PlayerModifier Next;
        public PlayerModifier(Player player)
        {
            _player = player;
        }
        public void Add(PlayerModifier cm)
        {
            if (Next != null)
            {
                Next.Add(cm);
            }
            else
            {
                Next = cm;
            }
        }
        public virtual void Handle() => Next?.Handle();
    }
}