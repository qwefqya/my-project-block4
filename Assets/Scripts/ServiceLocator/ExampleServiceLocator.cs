using System;
using System.Collections;
using System.Collections.Generic;
using ObjectPool;
using UnityEngine;

namespace Asteroids.ServiceLocator
{
    internal sealed class ExampleServiceLocator : MonoBehaviour
    {

        [SerializeField] GameObject _gameObject;
        private void Awake()
        {
            ServiceLocator.SetService<IViewServices>(new ViewServices());
        }
        private void Update()
        {
            ServiceLocator.Resolve<IViewServices>().Instantiate<Rigidbody2D>(_gameObject);
        }
    }
}
