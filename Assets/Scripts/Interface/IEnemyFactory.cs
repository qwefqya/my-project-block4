namespace Asteroids
{
   public interface IEnemyFactory
    {
        Enemy Create(Health hp, float force, float horizontal, float vertical);
    }
}