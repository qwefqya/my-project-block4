namespace Asteroids
{
    public interface IInteractable
    {
       bool IsInteractable { get; }
    }
}