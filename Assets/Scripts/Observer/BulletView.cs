using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asteroids
{
    public class BulletView : MonoBehaviour
    {
        public event Action<Collider2D> OnTriggerEnterChange;

        private void OnCollider2DEnter(Collider2D other)
        {
            OnTriggerEnterChange?.Invoke(other);
        }

       
    }
}