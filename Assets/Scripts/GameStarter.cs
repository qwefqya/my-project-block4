
using UnityEngine;
namespace Asteroids.Facade
{
    internal sealed class GameStarter : MonoBehaviour
    {

        private Player player;
       
        private void Start()
        {
            
            var gameServices = new GameServices();
            gameServices.Start();


            //Enemy.CreateAsteroidEnemy2Type(new Health(100.0f, 100.0f));
            //Enemy.CreateEnemyShip(new Health(100.0f, 100.0f));

            //IEnemyFactory factory = new AsteroidFactory();
            //factory.Create(new Health(100.0f, 100.0f), 1f, 1f, 1f);

            player = GameObject.FindObjectOfType<Player>();
            var root = new PlayerModifier(player);
            root.Add(new AddHPModifier(player, 250));
            root.Handle();

        }
    }
}