using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Asteroids
{
    public class Main : MonoBehaviour
    {
        private ListExecuteObject _interactiveObject;
        private InputController _inputController;
        private BulletHitController _bulletHitController;

        [SerializeField]  private PanelScore _panelScore;

        [SerializeField] private GameObject _player;
        [SerializeField] private GameObject _bulletView;

        public float _hp { get; internal set; }

        void Awake()
        {
            _inputController = new InputController(_player.GetComponent<Unit>());
            _bulletHitController = new BulletHitController(_bulletView.GetComponent<BulletView>());

            _interactiveObject = new ListExecuteObject();

            _interactiveObject.AddExecuteObject(_inputController);

            _interactiveObject.AddExecuteObject(_bulletHitController);

            _interactiveObject[0].Update();

            _interactiveObject[1].Update();

        }

        

        private void Update()
        {
            var _score = _inputController._score;
            _panelScore.Display(_score);

            for (int i = 0; i < _interactiveObject.Lenght; i++)
            {
                if (_interactiveObject[i] == null)
                {
                    continue;
                }

                _interactiveObject[i].Update();
            }



        }
    }
}
