using UnityEngine;

namespace Asteroids

{
    public sealed class StateTest : MonoBehaviour
    {
        [SerializeField] Color _colorGreen;
        [SerializeField] Color _colorRed;
        private Player _player;
        
        private void Start()
        {
            Context c = new Context(new StateRed());
            
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.transform.GetComponentInParent<Player>())
            {
                var _player = other.transform.GetComponentInParent<Player>();
                Context c = new Context(new StateGreen());
                c.Request();
                _player.ColorImplementation(_colorGreen);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.transform.GetComponentInParent<Player>())
            {
                var _player = other.transform.GetComponentInParent<Player>();
                Context c = new Context(new StateRed());
                c.Request();
                _player.ColorImplementation(_colorRed);
            }
        }
    }
}

