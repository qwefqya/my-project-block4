using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Asteroids
{
    public sealed class StateGreen : State
    {
        public override void Handle(Context context)
        {
            context.State = new StateGreen();
        }
    }

}