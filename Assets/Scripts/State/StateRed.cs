using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Asteroids
{
    public sealed class StateRed : State
    {
        public override void Handle(Context context)
        {
            context.State = new StateRed();
        }
    }

}