using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Asteroids
{
    public class BulletHitController : IExecute
    {

        private readonly BulletView _bulletView;

        public BulletHitController(BulletView view)
        {
            _bulletView = view;
            _bulletView.OnTriggerEnterChange += ViewOnTriggerEnterChange;
        }
        public void Update()
        {
            throw new System.NotImplementedException();
        }

        private void ViewOnTriggerEnterChange (Collider2D obj)
        {
            Debug.Log(obj.name);
        }

        public void Dispose()
        {
            _bulletView.OnTriggerEnterChange -= ViewOnTriggerEnterChange;
        }
    }
}