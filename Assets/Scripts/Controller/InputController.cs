using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asteroids
{
    public class InputController: IExecute
    {
        private readonly Unit _player;
        private float horizontal;
        private float vertical;
        private Camera _camera = Camera.main;
       
        public int _score { get; protected set; }
      

        public InputController(Unit player)
        {
            _player = player;   

        }

       


        public void Update()
        {
            
            var direction = Input.mousePosition - _camera.WorldToScreenPoint(_player.transform.position);
            horizontal = Input.GetAxis("Horizontal");
            vertical = Input.GetAxis("Vertical");

            _player.Move(horizontal, vertical);
            _player.Rotate(direction);

            if (Input.GetKeyDown(KeyCode.E))
            {
                _player.SwitchUnlockWeapon();
            }

            if (Input.GetButtonDown("Fire1"))
            {
                _player.FirePlayer();
                _score = _score + 100;
               
                //_panelScore.Display(_score);
                //������ ���� ���������� ���������� ����� �� ����� �������
            }



        }
    }
}