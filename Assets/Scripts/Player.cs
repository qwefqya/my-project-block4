using System;
using UnityEngine;
using ObjectPool;
using System.Collections;
using System.Collections.Generic;

namespace Asteroids
{
    internal sealed class Player : Unit
    {
        [SerializeField] private float _speed;
         private float _acceleration;
        //[SerializeField] private float _hp;
        [SerializeField] private Rigidbody2D _prefabbullet;
        [SerializeField] private Transform _barrel;
        [SerializeField] private float _force;
        [SerializeField] private Collider2D _collider;

        [SerializeField] private Sprite _sprite;
        private Rigidbody2D _rbbullet;

        private Camera _camera;
        private Rigidbody2D _rb;
        private IMove _moveTransform;
        private IRotation _rotation;
        private IViewServices _viewServices;

        private  UnlockWeapon _unlockWeapon;

        public float _hp { get; internal set; }

       // public Color _color { get; internal set; }

        //public Player(float force, float HP, float speed)
        //{
        //    _force = force;
        //    _hp = HP;
        //    _speed = speed;
        //}        //

        private void Awake()
        {

            
            _unlockWeapon = new UnlockWeapon(true);
            

            _viewServices = new ViewServices();
            _camera = Camera.main;
            _moveTransform = new AccelerationMove(transform, _speed, _acceleration);
            _rotation = new RotationShip(transform);
            _rb= GetComponent<Rigidbody2D>();
            if (_rb==null)
            {
                throw new Exception("Player No Rigitbody");
            }    
            _collider = GetComponent<Collider2D>();
            if (_collider==null)
            {
                throw new Exception("Player No Collider");
            }

           

            
        }



        public void ColorImplementation(Color color)
        {
            
            GetComponent<SpriteRenderer>().color = color;
            
        }

        public override void Move(float x, float y)
        {
            var speed = Time.deltaTime * _speed;
            _rb.AddRelativeForce((new Vector2(x, y) * speed ), ForceMode2D.Impulse);
         
        }


        public override void SwitchUnlockWeapon() 
        {
            if (_unlockWeapon.IsUnlock) _unlockWeapon.IsUnlock = false;
            else _unlockWeapon.IsUnlock = true;
        }
        public override void Rotate(Vector3 rotate)
        {

            _rotation.Rotation(rotate);
        }

        public override void FirePlayer()
        {
            Fire(_prefabbullet, _barrel, _force);
          
        }

        
        public override void Fire(Rigidbody2D bullet, Transform launchpoint, float force)
        {
           
            bullet = _viewServices.Instantiate<Rigidbody2D>(_prefabbullet.gameObject);
            bullet.transform.position = launchpoint.transform.position;
            bullet.transform.rotation = launchpoint.transform.rotation;
            bullet.AddForce((launchpoint.transform.position - transform.position) * force);
           // Debug.Log(_hp);
            _viewServices.Destroy(bullet.gameObject);


            if (_unlockWeapon.IsUnlock)
            {
                var BulletGO = new
               GameObject().SetName("Bullet").AddBoxCollider2D().AddBoxCollider2D().AddRigidbody2D
               (5.0f).AddSprite(_sprite);
                _rbbullet = BulletGO.GetComponent<Rigidbody2D>();
                BulletGO.transform.position = launchpoint.transform.position;
                BulletGO.transform.rotation = launchpoint.transform.rotation;
                _rbbullet.gravityScale = 0f;
                _rbbullet.AddForce((launchpoint.transform.position - transform.position) * force);
                Destroy(BulletGO, 4f);
            }
            else
            {
                Debug.Log("Weapon is lock");
            }


        }



        public void  OnCollisionEnter2D(Collision2D other)
        {
            var _obj = other.transform.GetComponentInParent<Asteroid>();
            if (_obj != null)
            {
                var dmg = _obj.TakeDamage();
                if (_hp <= 0)
                {
                    Destroy(gameObject);
                }
                else
                {
                    _hp = _hp - dmg;
                }
            }
        }
    }
}



