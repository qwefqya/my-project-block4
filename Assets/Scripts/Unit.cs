using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asteroids
{
    public abstract class Unit : MonoBehaviour
    {
       

        public abstract void Move(float x, float y);
        public abstract void Rotate(Vector3 Rotate);

        public abstract void Fire(Rigidbody2D bullet, Transform launchpoint, float force);

        public abstract void FirePlayer();

        public abstract void SwitchUnlockWeapon();


    }
}