using UnityEngine;
using UnityEngine.UI;
namespace Asteroids
{
    internal sealed class PanelHP : BaseUI
    {
        [SerializeField] private Text _text;
        [SerializeField] private Player _player;
        public override void Execute()
        {

            var _hp = _player._hp;
            _text.text = _hp.ToString();
            gameObject.SetActive(true);
        }
        public override void Cancel()
        {
            gameObject.SetActive(false);
        }
    }
}