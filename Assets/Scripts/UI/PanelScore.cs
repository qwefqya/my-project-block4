using System;
using UnityEngine;
using UnityEngine.UI;
namespace Asteroids
{
    internal sealed class PanelScore : BaseUI
    {
        [SerializeField] private Text _text;


        public override void Execute()
        {
          //  _text.text = nameof(PanelScore);
            gameObject.SetActive(true);
        }
        public override void Cancel()
        {
            gameObject.SetActive(false);
        }

        private string Interpret(string value)
        {
            if (Int64.TryParse(value, out var number))
            {
                return ToOrder(number);
            }
            else
                return null;
            
        }


        public void Display(int value)
        {
           // _text.text = $"Score: {value}";
            _text.text = Interpret ($"{value}");
        }

        private string ToOrder(long number)
        {
            
            if (number < 1) return string.Empty;
            if ((number >= 1) & (number < 1000)) return ($"{number}");
            if ((number >= 1000) & (number <1000000)) return ($"{number / 1000}K");

            if (number > 1000000) return ($"{number / 1000000}");

            
            throw new ArgumentOutOfRangeException(nameof(number));
        }


    }
}