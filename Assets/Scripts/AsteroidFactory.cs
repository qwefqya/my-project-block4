using UnityEngine;
namespace Asteroids
{
    internal sealed class AsteroidFactory : IEnemyFactory
    {
        public Enemy Create(Health hp, float force, float horizontal, float vertical)
        {
            var enemy = Object.Instantiate(Resources.Load<Asteroid>("Enemy/Asteroid"));

            enemy.DependencyInjectHealth(hp);
            var _dir = new Vector2(horizontal, vertical);
            enemy.Move(force, _dir);
            return enemy;
        }

    }
}