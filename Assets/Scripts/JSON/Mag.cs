using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asteroid.JSON
{
    public sealed class Mag : IUnit
    {
      public string Type { get; }
      public int Health { get; }

        public Mag (string type, int health)
        {
            Type = type;
            Health = health;
        }
    }
}