using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Asteroid.JSON
{
    public class JSONStarter : MonoBehaviour
    {
        private string _playerType;
        private int _playerHealth;

        private ICompositeFactory _unitFactory;

        public void SetUnitFactory (ICompositeFactory unitFactory)
        {
            _unitFactory = unitFactory;
        }
        void Start()
        {
            IUnit player = _unitFactory.Create(_playerType, _playerHealth);
        }


    }
}