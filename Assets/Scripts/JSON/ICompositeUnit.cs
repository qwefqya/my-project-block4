using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asteroid.JSON
{
    public interface ICompositeUnit
    {
        IUnit Mag { get; }
        IUnit Infantry { get; }
    }
}