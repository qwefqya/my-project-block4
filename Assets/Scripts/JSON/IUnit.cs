using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asteroid.JSON
{
    public interface IUnit
    {
        public string Type { get; }
        public int Health { get; }

    }
}