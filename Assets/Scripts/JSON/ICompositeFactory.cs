using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asteroid.JSON
{
    public interface ICompositeFactory
    {
        public IUnit Create(string type, int health);

    }
   
}