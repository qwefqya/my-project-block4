using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asteroid.JSON
{
    public sealed class Infantry : IUnit
    {
      public string Type { get; }
      public int Health { get; }

        public Infantry (string type, int health)
        {
            Type = type;
            Health = health;
        }
    }
}