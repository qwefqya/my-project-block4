using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asteroid.JSON
{
    public sealed class CompositeFactory
    {
        private readonly MagFactory _magFactory;
        private readonly InfantryFactory _infatryFactory;

        public CompositeFactory()
        {
            _magFactory = new MagFactory();
            _infatryFactory = new InfantryFactory();
        }

        public CompositeUnit Create (string unitType, int Health )
        {
            return new CompositeUnit(_magFactory.CreatePlayer(unitType, Health), _infatryFactory.CreatePlayer(unitType, Health));
        }
    }
   
}