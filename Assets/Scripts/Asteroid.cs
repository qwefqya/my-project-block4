using UnityEngine;

namespace Asteroids
{
    public sealed class Asteroid : Enemy
    {
        [SerializeField] private float damage = 20;
        [SerializeField] Player  _player;



        public float TakeDamage ()
        {
            return damage;
        }

     
    }
}
